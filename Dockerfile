# Use the official Alpine Linux image as the base image
# Используем официальный образ Alpine Linux в качестве базового образа
# 使用官方的Alpine Linux镜像作为基础镜像
FROM alpine:latest

# Execute commands as root
# Выполнять команды от имени root
# 以 root 用户执行命令
USER root

# Add the testing repository before updating packages
# Добавление тестового репозитория перед обновлением пакетов
# 在更新软件包之前添加测试存储库
RUN echo "https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

# Update the package manager and install shadowsocks-libev and tar
# Обновляем пакетный менеджер и устанавливаем shadowsocks-libev и tar
# 更新软件包管理器并安装 shadowsocks-libev 和 tar
RUN apk update && \
    apk add --no-cache shadowsocks-libev tar && \
    apk add --no-cache curl

# Download and extract v2ray-plugin
# Скачивание и разархивирование v2ray-plugin
# 下载和解压 v2ray-plugin
RUN curl -sSL "https://github.com/shadowsocks/v2ray-plugin/releases/download/v1.3.2/v2ray-plugin-linux-amd64-v1.3.2.tar.gz" | tar -xzC /usr/bin/ && \
    mv /usr/bin/v2ray-plugin_linux_amd64 /usr/bin/v2ray-plugin

# We remove the tar and curl utilities to reduce the container size
# Удаляем утилиты tar и curl, чтобы уменьшить размер контейнера
# 我们移除 tar 和 curl 工具，以减小容器的大小。
RUN apk del tar curl

# Create a directory for Shadowsocks configuration files
# Создание каталога для файлов конфигурации Shadowsocks
# 创建一个目录用于存放 Shadowsocks 配置文件
RUN mkdir -p /etc/shadowsocks

# Copy the shadowsocks.json configuration file to the directory
# Копируем файл конфигурации shadowsocks.json в каталог /etc/shadowsocks
# 将 shadowsocks.json 配置文件复制到目录中
COPY shadowsocks.json /etc/shadowsocks/config.json

# Define parameters that will be used to specify Shadowsocks settings when the container starts
# Определение параметров, которые будут использоваться для указания параметров Shadowsocks при запуске контейнера
# 定义参数，这些参数将在容器启动时用于指定 Shadowsocks 设置
ENV SHADOWSOCKS_HOST=
ENV SHADOWSOCKS_PORT=
ENV SHADOWSOCKS_KEY=

# Replace values of parameters in the configuration file
# Замена значений параметров в конфигурационном файле
# 在配置文件中替换参数的值
CMD sed -i "s/{{ host_nodename }}/$SHADOWSOCKS_HOST/" /etc/shadowsocks/config.json && \
    sed -i "s/{{ shadowsocks_port_enable }}/$SHADOWSOCKS_PORT/" /etc/shadowsocks/config.json && \
    sed -i "s/{{ shadowsoks_password }}/$SHADOWSOCKS_KEY/" /etc/shadowsocks/config.json && \
    ss-server -c /etc/shadowsocks/config.json

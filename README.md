## Dockerfile для Shadowsocks с v2ray-plugin на Alpine Linux

Этот Dockerfile позволяет создать образ Docker для Shadowsocks с использованием v2ray-plugin на базе Alpine Linux. 
Shadowsocks - это инструмент для обхода цензуры в интернете с помощью прокси-сервера, а v2ray-plugin обеспечивает дополнительные возможности обфускации трафика.

### Использование

1. Убедитесь, что Docker и Git установлен на вашем компьютере.
2. Склонируйте репозиторий с Dockerfile:

    ```bash
    git clone https://gitlab.com/dante300814/shadowsocks-libev_docker.git
    ```

3. Перейдите в каталог с Dockerfile:

    ```bash
    cd repository
    ```

4. Создайте образ Docker:

    ```bash
    docker build -t my_shadowsocks_image .
    ```

5. Запустите контейнер с указанием параметров Shadowsocks:

    ```bash
    docker run -it -p "внешний порт":"внутренний порт SHADOWSOCKS_PORT" -e SHADOWSOCKS_HOST="0.0.0.0" -e SHADOWSOCKS_PORT="port" -e SHADOWSOCKS_KEY="passwor123" my_shadowsocks_image
    ```

### Параметры

- `SHADOWSOCKS_HOST`: IP-адрес или доменное имя сервера Shadowsocks.
- `SHADOWSOCKS_PORT`: Порт, используемый для подключения к серверу Shadowsocks.
- `SHADOWSOCKS_KEY`: Пароль для подключения к серверу Shadowsocks.

### Конфигурация

Файл конфигурации Shadowsocks находится в `/etc/shadowsocks/config.json` внутри контейнера Docker. Можно внести необходимые изменения в этот файл вручную.

### Примечание

При конфигурации клиента указывать внешний IP адрес хоста и внешний порт указанный при запуске контейнера.
Рекомендуется в параметре SHADOWSOCKS_HOST указывать IP адрес 0.0.0.0 для прослушивании всех хостов Docker пространства.
Настроить правильно перенаправление сетевых портов, для корректной работы и повышения безопасности.

### Лицензия

Этот Dockerfile распространяется под лицензией MIT. Смотрите файл LICENSE для получения дополнительной информации.

### Автор

Автор: jshotyk
Email: #######


### Благодарности

Особая благодарность команде Lain

### Дополнительная информация

Для получения более подробной информации о Shadowsocks и v2ray-plugin, пожалуйста, обратитесь к соответствующей документации:

- [Shadowsocks](https://github.com/shadowsocks)
- [v2ray-plugin](https://github.com/shadowsocks/v2ray-plugin)

---
*Этот README.md создан для Dockerfile Shadowsocks с v2ray-plugin на Alpine Linux*


## Dockerfile for Shadowsocks with v2ray-plugin on Alpine Linux

This Dockerfile allows you to create a Docker image for Shadowsocks with v2ray-plugin based on Alpine Linux. Shadowsocks is a tool for bypassing internet censorship using a proxy server, and v2ray-plugin provides additional traffic obfuscation capabilities.

### Usage

1. Make sure Docker and Git are installed on your computer.
2. Clone the repository with the Dockerfile:

    ```bash
    git clone https://gitlab.com/dante300814/shadowsocks-libev_docker.git
    ```

3. Navigate to the directory with the Dockerfile:

    ```bash
    cd repository
    ```

4. Build the Docker image:

    ```bash
    docker build -t my_shadowsocks_image .
    ```

5. Run the container specifying Shadowsocks parameters:

    ```bash
    docker run -it -p "external_port":"SHADOWSOCKS_PORT" -e SHADOWSOCKS_HOST="0.0.0.0" -e SHADOWSOCKS_PORT="port" -e SHADOWSOCKS_KEY="password123" my_shadowsocks_image
    ```

### Parameters

- `SHADOWSOCKS_HOST`: IP address or domain name of the Shadowsocks server.
- `SHADOWSOCKS_PORT`: Port used for connecting to the Shadowsocks server.
- `SHADOWSOCKS_KEY`: Password for connecting to the Shadowsocks server.

### Configuration

The Shadowsocks configuration file is located at `/etc/shadowsocks/config.json` inside the Docker container. You can make necessary changes to this file manually.

### Note

When configuring the client, specify the external IP address of the host and the external port specified when running the container.
It is recommended to specify the IP address 0.0.0.0 for the SHADOWSOCKS_HOST parameter to listen on all Docker host interfaces.
Configure port forwarding correctly for proper operation and increased security.

### License

This Dockerfile is distributed under the MIT License. See the LICENSE file for more information.

### Author

Author: jshotyk
Email: #######

### Acknowledgements

Special thanks to the Lain team.

### Additional Information

For more detailed information about Shadowsocks and v2ray-plugin, please refer to the respective documentation:

- [Shadowsocks](https://github.com/shadowsocks)
- [v2ray-plugin](https://github.com/shadowsocks/v2ray-plugin)

---
*This README.md is created for Dockerfile Shadowsocks with v2ray-plugin on Alpine Linux*



## 基于Alpine Linux的Shadowsocks与v2ray-plugin的Dockerfile

这个Dockerfile允许你基于Alpine Linux创建一个带有v2ray-plugin的Shadowsocks的Docker镜像。Shadowsocks是一个用于通过代理服务器绕过互联网审查的工具，而v2ray-plugin则提供了额外的流量混淆功能。

### 使用方法

1. 确保你的计算机已经安装了Docker和Git。
2. 克隆带有Dockerfile的仓库：

    ```bash
    git clone https://gitlab.com/dante300814/shadowsocks-libev_docker.git
    ```

3. 进入包含Dockerfile的目录：

    ```bash
    cd repository
    ```

4. 构建Docker镜像：

    ```bash
    docker build -t my_shadowsocks_image .
    ```

5. 运行容器并指定Shadowsocks的参数：

    ```bash
    docker run -it -p "外部端口":"SHADOWSOCKS_PORT" -e SHADOWSOCKS_HOST="0.0.0.0" -e SHADOWSOCKS_PORT="端口" -e SHADOWSOCKS_KEY="密码" my_shadowsocks_image
    ```

### 参数

- `SHADOWSOCKS_HOST`：Shadowsocks服务器的IP地址或域名。
- `SHADOWSOCKS_PORT`：连接到Shadowsocks服务器的端口。
- `SHADOWSOCKS_KEY`：连接到Shadowsocks服务器的密码。

### 配置

Shadowsocks的配置文件位于Docker容器内的`/etc/shadowsocks/config.json`。您可以手动对该文件进行必要的更改。

### 注意

在配置客户端时，指定主机的外部IP地址和在运行容器时指定的外部端口。
建议在`SHADOWSOCKS_HOST`参数中指定IP地址0.0.0.0，以监听Docker主机的所有接口。
正确配置端口转发以确保正确运行并提高安全性。

### 许可证

这个Dockerfile是根据MIT许可证分发的。有关更多信息，请参阅LICENSE文件。

### 作者

作者：jshotyk
电子邮件：#######

### 鸣谢

特别感谢Lain团队。

### 更多信息

有关Shadowsocks和v2ray-plugin的更多详细信息，请参阅相关文档：

- [Shadowsocks](https://github.com/shadowsocks)
- [v2ray-plugin](https://github.com/shadowsocks/v2ray-plugin)

---
*此README.md适用于基于Alpine Linux的Shadowsocks与v2ray-plugin的Dockerfile*
